package com.bilas.pizzeria;

import com.bilas.pizzeria.factory.Pizzeria;
import com.bilas.pizzeria.factory.city.KyivPizzeria;
import com.bilas.pizzeria.factory.city.LvivPizzeria;
import com.bilas.pizzeria.factory.city.OdesaPizeria;
import com.bilas.pizzeria.pizza.make.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

class Menu {
    private static final Logger LOGGER = LogManager.getLogger(Main.class);
    private static final Scanner SCANNER = new Scanner(System.in);

    private static void showPizza(Pizza pizza) {
        LOGGER.info(pizza);
    }

    private static Pizza getPizza(Pizzeria pizzeria) {
        LOGGER.info("add your pizza:");
        String p = SCANNER.nextLine();
        Pizza pizza = null;
        if (Type.BARBECUE.toString().equalsIgnoreCase(p)) {
            assert pizzeria != null;
            pizza = pizzeria.prepare(Type.BARBECUE);
        } else if (Type.EMILIA.toString().equalsIgnoreCase(p)) {
            assert pizzeria != null;
            pizza = pizzeria.prepare(Type.BARBECUE);
        } else if (Type.MEXICAN.toString().equalsIgnoreCase(p)) {
            assert pizzeria != null;
            pizza = pizzeria.prepare(Type.MEXICAN);
        } else if (Type.PEPERONI.toString().equalsIgnoreCase(p)) {
            assert pizzeria != null;
            pizza = pizzeria.prepare(Type.PEPERONI);
        } else if (Type.ROYAL.toString().equalsIgnoreCase(p)) {
            assert pizzeria != null;
            pizza = pizzeria.prepare(Type.ROYAL);
        }
        return pizza;
    }

    private static Pizzeria getPizzeria() {
        LOGGER.info("add your city:");
        String c = SCANNER.nextLine();
        Pizzeria pizzeria = null;
        if (City.KYIV.toString().equalsIgnoreCase(c)) {
            pizzeria = new KyivPizzeria();
        } else if (City.LVIV.toString().equalsIgnoreCase(c)) {
            pizzeria = new LvivPizzeria();
        } else if (City.ODESA.toString().equalsIgnoreCase(c)) {
            pizzeria = new OdesaPizeria();
        }
        return pizzeria;
    }

    void showMenu() {
        Pizzeria pizzeria = getPizzeria();
        Pizza pizza = getPizza(pizzeria);
        showPizza(pizza);
    }
}
