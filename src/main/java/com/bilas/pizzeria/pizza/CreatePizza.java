package com.bilas.pizzeria.pizza;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public interface CreatePizza {
    Logger LOGGER = LogManager.getLogger();
    void prepare();

    void bake();

    default void cut(){
        LOGGER.info("Cutting...");
        try {
            Thread.sleep(1300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    void box();

}
