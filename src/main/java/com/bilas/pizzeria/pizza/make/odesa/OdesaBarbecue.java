package com.bilas.pizzeria.pizza.make.odesa;

import com.bilas.pizzeria.pizza.make.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.List;

public class OdesaBarbecue extends Pizza {
    private static final Logger LOGGER = LogManager.getLogger(OdesaBarbecue.class);
    public OdesaBarbecue() {
        super("Barbecue ODESA", 130, "Thick", "marinara", addToppings());
    }

    private static List<String> addToppings() {
        List<String> list = new LinkedList<>();
        list.add("Bacon");
        list.add("Smoked sausages");
        list.add("Smoked chiken");
        list.add("Parsley");
        list.add("Mozarella");
        list.add("Smoked cheese");
        return list;
    }

    @Override
    public void prepare() {
        LOGGER.info("Preparing " + getName() + " pizza:");
        try {
            Thread.sleep(1300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void bake() {
        LOGGER.info("Baking...");
        try {
            Thread.sleep(2500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void box() {
        LOGGER.info("Boxing...");
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
