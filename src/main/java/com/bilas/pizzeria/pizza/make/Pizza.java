package com.bilas.pizzeria.pizza.make;

import com.bilas.pizzeria.pizza.CreatePizza;

import java.util.List;

public abstract class Pizza implements CreatePizza {
    private String name;
    private int price;
    private String dough;
    private String sauce;
    private List<String> toppings;

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public String getDough() {
        return dough;
    }

    public String getSauce() {
        return sauce;
    }

    public List<String> getToppings() {
        return toppings;
    }

    public Pizza(String name, int price, String dough, String sauce, List<String> toppings) {
        this.name = name;
        this.price = price;
        this.dough = dough;
        this.sauce = sauce;
        this.toppings = toppings;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", dough='" + dough + '\'' +
                ", sauce='" + sauce + '\'' +
                ", toppings=" + toppings +
                '}';
    }
}
