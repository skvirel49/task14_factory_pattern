package com.bilas.pizzeria.pizza.make.lviv;

import com.bilas.pizzeria.pizza.make.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.List;

public class LvivMexican extends Pizza {
    private static final Logger LOGGER = LogManager.getLogger(LvivMexican.class);

    public LvivMexican() {
        super("Mexican LVIV", 130, "Thick", "Barbecue sauce", addToppings());
    }

    private static List<String> addToppings() {
        List<String> list = new LinkedList<>();
        list.add("Olive oil");
        list.add("Mozarella");
        list.add("Pepperoni sausages");
        list.add("Red onion");
        list.add("Cherri tomato");
        list.add("Chicken fillet");
        list.add("Ground beef");
        return list;
    }

    @Override
    public void prepare() {
        LOGGER.info("Preparing " + getName() + " pizza:");
        try {
            Thread.sleep(1300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void bake() {
        LOGGER.info("Baking...");
        try {
            Thread.sleep(2500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void box() {
        LOGGER.info("Boxing...");
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
