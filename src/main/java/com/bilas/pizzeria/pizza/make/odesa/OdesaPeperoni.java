package com.bilas.pizzeria.pizza.make.odesa;

import com.bilas.pizzeria.pizza.make.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.List;

public class OdesaPeperoni extends Pizza {
    private static final Logger LOGGER = LogManager.getLogger(OdesaPeperoni.class);

    public OdesaPeperoni() {
        super("Peperoni ODESA", 150, "Thin", "Tomato sauce", addToppings());
    }

    private static List<String> addToppings() {
        List<String> list = new LinkedList<>();
        list.add("Olive oil");
        list.add("Basil");
        list.add("Mozarella");
        list.add("Peperoni");
        list.add("Bacon");
        list.add("Parsley");
        list.add("Smoked cheese");
        list.add("Olive oil");
        list.add("Salame");
        list.add("Mozarella");
        list.add("Edam cheese");
        return list;
    }

    @Override
    public void prepare() {
        LOGGER.info("Preparing " + getName() + " pizza:");
        try {
            Thread.sleep(1300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void bake() {
        LOGGER.info("Baking...");
        try {
            Thread.sleep(2500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void box() {
        LOGGER.info("Boxing...");
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
