package com.bilas.pizzeria.factory.city;

import com.bilas.pizzeria.Type;
import com.bilas.pizzeria.factory.Pizzeria;
import com.bilas.pizzeria.pizza.make.Pizza;
import com.bilas.pizzeria.pizza.make.odesa.*;

public class OdesaPizeria extends Pizzeria {
    @Override
    public Pizza createPizza(Type type) {
        Pizza pizza = null;

        if (type == Type.BARBECUE) {
            pizza = new OdesaBarbecue();
        } else if (type == Type.EMILIA) {
            pizza = new OdesaEmilia();
        } else if (type == Type.MEXICAN) {
            pizza = new OdesaMexican();
        } else if (type == Type.PEPERONI) {
            pizza = new OdesaPeperoni();
        } else if (type == Type.ROYAL) {
            pizza = new OdesaRoyal();
        }
        return pizza;
    }
}
