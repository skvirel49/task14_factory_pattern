package com.bilas.pizzeria.factory.city;

import com.bilas.pizzeria.Type;
import com.bilas.pizzeria.factory.Pizzeria;
import com.bilas.pizzeria.pizza.make.Pizza;
import com.bilas.pizzeria.pizza.make.lviv.*;

public class LvivPizzeria extends Pizzeria {
    @Override
    public Pizza createPizza(Type type) {
        Pizza pizza = null;

        if (type == Type.BARBECUE) {
            pizza = new LvivBarbecue();
        } else if (type == Type.EMILIA) {
            pizza = new LvivEmilia();
        } else if (type == Type.MEXICAN) {
            pizza = new LvivMexican();
        } else if (type == Type.PEPERONI) {
            pizza = new LvivPeperoni();
        } else if (type == Type.ROYAL) {
            pizza = new LvivRoyal();
        }
        return pizza;
    }
}
