package com.bilas.pizzeria.factory.city;

import com.bilas.pizzeria.Type;
import com.bilas.pizzeria.factory.Pizzeria;
import com.bilas.pizzeria.pizza.make.Pizza;
import com.bilas.pizzeria.pizza.make.kyiv.*;

public class KyivPizzeria extends Pizzeria {
    @Override
    public Pizza createPizza(Type type) {
        Pizza pizza = null;

        if (type == Type.BARBECUE) {
            pizza = new KyivBarbecue();
        } else if (type == Type.EMILIA) {
            pizza = new KyivEmilia();
        } else if (type == Type.MEXICAN) {
            pizza = new KyivMexican();
        } else if (type == Type.PEPERONI) {
            pizza = new KyivPeperoni();
        } else if (type == Type.ROYAL) {
            pizza = new KyivRoyal();
        }
        return pizza;
    }

}
