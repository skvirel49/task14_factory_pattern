package com.bilas.pizzeria.factory;

import com.bilas.pizzeria.City;
import com.bilas.pizzeria.Type;
import com.bilas.pizzeria.pizza.make.Pizza;

public abstract class Pizzeria {

    public abstract Pizza createPizza(Type type);

    public Pizza prepare(Type type) {
        Pizza pizza = createPizza(type);
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();
        return pizza;
    }
}
